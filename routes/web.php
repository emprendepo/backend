<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{UserController, AuthController, UserPortalController, PedidosController, SuscripcionesController, EmpresaController, GenericController, CarroCompraController, BoletaController, FileController, ProductosController, PagosController};

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::controller(PedidosController::class)->group(function () {
    Route::get('/despacho/{id}', 'generateEnvio');
});


Route::prefix('api/public')->group(function () {

    Route::prefix('test')->controller(UserController::class)->group(function () {
        Route::post('/mail', 'testMail');
    });

    Route::controller(UserController::class)->group(function () {
        Route::post('/user/validate/token', 'validateToken');
        Route::post('/user/validate/mail', 'validateMail');
    });

    Route::middleware('auth:public')->group(function () {
        Route::prefix('user')->controller(UserPortalController::class)->group(function () {
            Route::prefix('profile')->group(function () {
                Route::put('/', 'update');
            });
            Route::prefix('direcciones')->group(function () {
                Route::get('/', 'meDirecciones');
                Route::post('/', 'addDireccion');
                Route::delete('/{id}', 'deleteDireccion');
            });
            Route::prefix('compras')->group(function () {
                Route::get('/', 'getCompras');
            });
        });
    });

    Route::prefix('pyme')->controller(EmpresaController::class)->group(function () {
        Route::get('/{id}', 'show');
        Route::post('/', 'storePublic');
    });
    Route::prefix('transaction')->controller(PagosController::class)->group(function () {
        Route::post('/create', 'create');
        Route::get('/result', 'result');
    });

    Route::prefix('suscripcion')->controller(SuscripcionesController::class)->group(function () {
        Route::get('/result', 'result');
    });

    Route::prefix('auth')->controller(AuthController::class)->group(function () {
        Route::post('login', 'loginPublic');
        Route::middleware('auth:public')->group(function () {
            Route::get('me', 'mePublic');
        });
    });

    Route::prefix('productos')->controller(ProductosController::class)->group(function () {
        Route::get('/', 'index');
        Route::post('/filter', 'filter');
        Route::get('/categorias', 'categorias');
    });

    Route::prefix('carro')->controller(CarroCompraController::class)->group(function () {
        Route::post('/list', 'show');
        Route::post('/add', 'store');
        Route::post('/delete', 'destroy');
    });

    Route::prefix('Usuario')->controller(UserPortalController::class)->group(function () {
        Route::post('/validate/token', 'validateToken');
        Route::post('/', 'store');
    });
});

Route::prefix('api/auth')->controller(AuthController::class)->group(function () {
    Route::post('login', 'login');
    Route::middleware('auth:web')->group(function () {
        Route::post('logout', 'logout');
        Route::post('refresh', 'refresh');
        Route::get('me', 'me');
    });
});

Route::prefix('api')->middleware('auth:web')->group(function () {
    Route::prefix('pyme')->controller(EmpresaController::class)->group(function () {
        Route::get('/dashboard', 'index');
        Route::post('/informes', 'informes');
        Route::put('/empresa/{id}', 'update');
    });

    Route::prefix('pyme')->controller(FileController::class)->group(function () {
        Route::post('/logos', 'logos');
    });

    Route::prefix('pyme/suscripcion')->controller(SuscripcionesController::class)->group(function () {
        Route::post('/', 'validateSuscripcion');
        Route::delete('/', 'delete');
    });
    Route::prefix('pyme/pedidos')->controller(PedidosController::class)->group(function () {
        Route::put('/{id}', 'update');
    });

    Route::prefix('entidad')->controller(GenericController::class)->group(function () {
        Route::get('/{entidad}', 'index');
        Route::get('/{entidad}/list', 'list');
        Route::get('/{entidad}/form', 'form');
        Route::post('/{entidad}', 'store');
        Route::put('/{entidad}/{id}', 'update');
        Route::delete('/{entidad}/{id}', 'destroy');
    });
    Route::prefix('boleta')->controller(BoletaController::class)->group(function () {
        Route::get('/form', 'index');
        Route::get('/detalle/{id}', 'getId');
        Route::post('/form', 'store');
    });

    Route::prefix('empresas')->controller(EmpresaController::class)->group(function () {
        Route::get('/', 'list');
        Route::get('/form', 'form');
        Route::post('/', 'store');
        Route::put('/{id}', 'updateForm');
        Route::delete('/{id}', 'destroy');
    });

    Route::prefix('User')->controller(UserController::class)->group(function () {
        Route::get('/validate/token', 'list');
        Route::get('/', 'list');
        Route::get('/form', 'form');
        Route::post('/', 'store');
        Route::post('/update', 'update');
        Route::put('/{id}', 'updateForm');
        Route::delete('/{id}', 'destroy');
    });

    Route::prefix('files')->controller(FileController::class)->group(function () {
        Route::post('/', 'store');
    });
});
