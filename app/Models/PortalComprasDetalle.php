<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PortalComprasDetalle extends Model
{
    protected $table = 'portal_compras_usuarios_detalle';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ["portal_compras_id", "emp_productos_id", "cantidad", "valor"];


    public function Productos()
    {
        return $this->belongsTo('App\Models\Productos', 'emp_productos_id', 'id');
    }
}
