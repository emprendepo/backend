<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarroCompra extends Model
{
    use SoftDeletes;

    protected $table = 'portal_carros_de_compras';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['uuid', 'usuario_id'];

    public function CarroCompraDetalle()
    {
        return $this->hasMany('App\Models\CarroCompraDetalle', 'carros_de_compras_id');
    }
}
