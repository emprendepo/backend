<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class DireccionesUsuarios extends Model
{
    protected $table = 'portal_usuarios_direcciones';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['descripcion', 'calle', 'comuna', 'region', 'usuario_id'];
}
