<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PortalCompras extends Model
{

    protected $table = 'portal_compras_usuarios';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ["portal_boletas_id", "direccion_id"];


    public function Detalle()
    {
        return $this->hasMany('App\Models\PortalComprasDetalle', 'portal_compras_id', 'id');
    }

    public function Direccion()
    {
        return $this->hasOne('App\Models\DireccionesUsuarios', 'id', 'direccion_id');
    }
}
