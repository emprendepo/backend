<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CarroCompraDetalle extends Model
{
    use SoftDeletes;

    protected $table = 'portal_carros_de_compras_detalle';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['carros_de_compras_id', 'emp_productos_id', 'cantidad'];

    public function Productos()
    {
        return $this->belongsTo('App\Models\Productos', 'emp_productos_id', 'id');
    }
}
