<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    protected $table = 'emp_clientes';


    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'rut' => 'required',
            'nombres' => 'required',
            'apellido_paterno' => 'required',
            'apellido_materno' => 'required',
        ];
    }

    protected $fillable = ['rut', 'nombres', 'apellido_paterno', 'apellido_materno', 'direccion', 'telefono', 'email', 'empresas_id'];


    public function table()
    {
        return strtolower($this->table);
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public static function data($request, &$roleData, $user)
    {
        $roleData->rut = $request->has('rut') ? $request->rut : 0;
        $roleData->nombres = $request->has('nombres') ? $request->nombres : 0;
        $roleData->apellido_paterno = $request->has('apellido_paterno') ? $request->apellido_paterno : 0;
        $roleData->apellido_materno = $request->has('apellido_materno') ? $request->apellido_materno : 0;
        $roleData->direccion = $request->has('direccion') ? $request->direccion : 0;
        $roleData->telefono = $request->has('telefono') ? $request->telefono : 0;
        $roleData->email = $request->has('email') ? $request->email : '';
        $roleData->empresas_id = $user->empresas->id;
    }

    public function isVisible()
    {
        return ['empresas_id'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombres as label'];
    }
}
