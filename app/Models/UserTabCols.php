<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTabCols extends Model
{
    protected $table = 'information_schema.columns';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

}
