<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emp_categorias';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];


    public static function rules()
    {
        return [
            'name' => 'required|max:20',
            'descripcion' => 'required|max:200',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData, $user)
    {
        $roleData->name = $request->has('name') ? $request->name : "";
        $roleData->descripcion = $request->has('descripcion') ? $request->descripcion : "";
        $roleData->empresas_id = $user->empresas->id;
    }

    public function isVisible()
    {
        return ['empresas_id'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'name as label'];
    }
}
