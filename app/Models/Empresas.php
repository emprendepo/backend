<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use App\Utils\{Rut};
use Log;

class Empresas extends Model
{
    use SoftDeletes;

    protected $table = 'emp_empresas';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'name' => 'required',
        ];
    }


    public function table()
    {
        return strtolower($this->table);
    }

    protected $fillable = ['name', 'descripcion', 'logo', 'fondo', 'facebook', 'instragram', 'tiktok', 'direccion', 'region', 'comuna', 'telefono', 'celular', 'email', 'sitio'];


    public function Productos()
    {
        return $this->hasMany('App\Models\Productos', 'empresas_id', 'id');
    }

    public function Suscripciones()
    {
        return $this->hasOne('App\Models\Suscripciones', 'empresas_id', 'id');
    }

    public function isVisible()
    {
        return ['type', 'descripcion', 'logo', 'fondo', 'token_inscripcion', 'facebook', 'instragram', 'tiktok', 'direccion', 'region', 'comuna', 'telefono', 'celular'];
    }

    public static function data($request, &$roleData, $user)
    {
        $roleData->rut = Rut::cleanRut($request->rut) ?? "";
        $roleData->name = $request->name ?? "";
        $roleData->email = $request->email ?? "";
        $roleData->sitio = $request->sitio ?? null;
    }

    public static function mapColumns()
    {
        return ['id as value', 'name as label'];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }
}
