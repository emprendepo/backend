<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 * @package App\Models\Role
 */
class Role extends Model
{
    protected $table = 'roles';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    public static function rules()
    {
        return [
            'nombre' => 'required|string',
            'name' => 'required|string',
            'guard_name' => 'required|string',
            'descripcion' => 'required|string',

        ];
    }

    protected $fillable = ['nombre', 'descripcion'];

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'link_user_roles', 'role_id', 'user_id');
    }
}
