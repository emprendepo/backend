<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Files extends Model
{

    protected $table = 'emp_files';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];


    public function table()
    {
        return strtolower($this->table);
    }

    public static function mapColumns()
    {
        return ['id as value', 'name as label'];
    }
}
