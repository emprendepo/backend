<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PortalBoletas extends Model
{
    protected $table = 'portal_boletas';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ["folio", "monto_total", "cantidades", "fecha_de_compra", "usuario_id"];


    public function Compras()
    {
        return $this->hasOne('App\Models\PortalCompras', 'portal_boletas_id', 'id');
    }
}
