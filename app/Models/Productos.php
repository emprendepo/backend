<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Log;

class Productos extends Model
{
    protected $table = 'emp_productos';


    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'nombre' => 'required',
            'descripcion' => 'required',
            'numero_serie' => 'required',
            'valor' => 'required',

        ];
    }

    protected $fillable = ['nombre', 'descripcion', 'numero_serie', 'valor'];

    public function table()
    {
        return strtolower($this->table);
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public static function data($request, &$roleData, $user)
    {
        $roleData->nombre = $request->has('nombre') ? $request->nombre : 0;
        $roleData->descripcion = $request->has('descripcion') ? $request->descripcion : 0;
        $roleData->numero_serie = $request->has('numero_serie') ? $request->numero_serie : 0;
        $roleData->valor = $request->has('valor') ? $request->valor : 0;
        $roleData->categoria_id = $request->has('categoria_id') ? $request->categoria_id : 0;
        $roleData->empresas_id = $user->empresas->id;
    }

    public static function dataSync($request, &$Producto)
    {
        if ($request->has('files')) {
            $files = $request->get('files');
            $files = collect($files)->pluck("id") ?? $files;
            $Producto->Files()->sync([]);
            $Producto->Files()->sync($files);
        }
    }

    public function isVisible()
    {
        return ['empresas_id'];
    }

    public function empresas()
    {
        return $this->hasOne(Empresas::class, 'id', 'empresas_id');
    }

    public function Categoria()
    {
        return $this->hasOne(Categoria::class, 'id', 'categoria_id');
    }

    public function Files()
    {
        return $this->belongsToMany('App\Models\Files', 'link_file_producto', 'producto_id', 'file_id');
    }

    public function withTable()
    {
        return ['Categoria'];
    }

    public function withTableMany()
    {
        return ['Files'];
    }

    public function FileUpload()
    {
        return ['Files'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombre as label'];
    }
}
