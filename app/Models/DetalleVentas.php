<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleVentas extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emp_detalle_ventas';


    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'ventas_id' => 'required',
            'productos_id' => 'required',
            'cantidad' => 'required',
            'valor' => 'required',
        ];
    }
    protected $fillable = ['ventas_id', 'productos_id', 'cantidad', 'valor'];

    public function Ventas()
    {
        return $this->belongsTo('App\Models\Ventas', 'id', 'boletas_id');
    }

    public function Productos()
    {
        return $this->belongsTo('App\Models\Productos', 'productos_id', 'id');
    }
}
