<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{

    protected $table = 'table_pedidos';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['boletas_id', 'empresas_id', 'cantidades', 'valor_total', 'direccion', 'estado'];

    public function table()
    {
        return strtolower($this->table);
    }

    public function isVisible()
    {
        return ['empresas_id', 'boletas_id'];
    }

    public function Boletas()
    {
        return $this->hasOne('App\Models\Boletas', 'id', 'boletas_id');
    }

    public function withTable2()
    {
        return ['Boletas.metodoDePago', 'Boletas.Clientes', 'Boletas.Ventas.DetalleVentas.Productos', 'Boletas.Ventas.DetalleVentas.Productos.Categoria', 'Boletas.Ventas.DetalleVentas.Productos.Files'];
    }
}
