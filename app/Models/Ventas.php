<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ventas extends Model
{
    protected $table = 'emp_ventas';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    public static function rules()
    {
        return [
            'boletas_id' => 'required',
            'fecha_creacion' => 'required',
        ];
    }

    protected $fillable = ['boletas_id', 'fecha_creacion', 'empresas_id'];


    public function table()
    {
        return strtolower($this->table);
    }


    public static function data($request, &$roleData, $user)
    {
        $roleData->facturas_id = $request->has('boletas_id') ? $request->facturas_id : 0;
        $roleData->fecha_creacion = $request->has('fecha_creacion') ? $request->fecha_creacion : 0;
        $roleData->empresas_id = $user->empresas->id;
    }


    public function Boletas()
    {
        return $this->belongsTo('App\Models\Boletas', 'boletas_id', 'id');
    }

    public function DetalleVentas()
    {
        return $this->hasMany('App\Models\DetalleVentas');
    }

    public function isVisible()
    {
        return ['empresas_id'];
    }

    public function withTable()
    {
        return ['Boletas'];
    }
}
