<?php

namespace App\Models;

use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable implements JWTSubject
{

    use SoftDeletes;

    use HasApiTokens, HasFactory, Notifiable;


    protected $fillable = [
        'name',
        'email',
        'password',
    ];


    protected $hidden = [
        'password',
        'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
        'is_blocked' => 'boolean'
    ];

    public static function rules()
    {
        return [
            'name' => 'required',
        ];
    }


    public static function data($request, &$roleData, $user)
    {
        $roleData->name = $request->name ?? "";
        $roleData->email = $request->email ?? "";
        $roleData->empresas_id = $request->empresas_id ?? "";
        $roleData->remember_token = $request->remember_token ?? "";
        $roleData->is_blocked = $request->is_blocked ?? false;
    }

    public function table()
    {
        return strtolower("users");
    }


    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function empresas()
    {
        return $this->hasOne(Empresas::class, 'id', 'empresas_id');
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role', 'link_user_roles', 'user_id', 'role_id')->orderBy('role_id');
    }

    public function isVisible()
    {
        return ['is_admin', 'remember_token', 'email_verified_at', 'password'];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function withTable()
    {
        return ['empresas'];
    }
}
