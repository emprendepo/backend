<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Suscripciones extends Model
{
    use SoftDeletes;

    protected $table = 'emp_suscripciones';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = ['response_code', 'tbk_user', 'authorization_code', 'card_type', 'card_number', 'empresas_id'];
}
