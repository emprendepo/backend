<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MetodoDePago extends Model
{
    //use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'emp_metodo_de_pago';

    protected $hidden = [
        'updated_at',
        'deleted_at',
    ];


    public static function rules()
    {
        return [
            'nombre' => 'required|max:20',
        ];
    }

    public static function formWhere()
    {
        return [
            'where' => false
        ];
    }

    public function table()
    {
        return strtolower($this->table);
    }

    public static function data($request, &$roleData, $user)
    {
        $roleData->nombre = $request->has('nombre') ? $request->nombre : 0;
        $roleData->empresas_id = $user->empresas->id;
    }

    public function isVisible()
    {
        return ['empresas_id'];
    }

    public static function mapColumns()
    {
        return ['id as value', 'nombre as label'];
    }
}
