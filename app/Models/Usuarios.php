<?php

namespace App\Models;

use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Usuarios extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = 'portal_usuarios';

    public function table()
    {
        return strtolower($this->table);
    }

    protected $fillable = [
        'nombres',
        'email',
        'password',
    ];

    public static function rules()
    {
        return [
            'nombres' => 'required',
            'email' => 'required',
        ];
    }


    protected $hidden = [
        'password',
        'remember_token',
    ];


    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function direcciones()
    {
        return $this->hasMany(DireccionesUsuarios::class, 'usuario_id', 'id');
    }

    public function isVisible()
    {
        return ['email_verified_at', 'password', 'remember_token'];
    }



    public static function data($request, &$roleData, $user)
    {
        $roleData->rut = $request->rut ?? "";
        $roleData->rut_dv = $request->rut_dv ?? $request->dv ?? "";
        $roleData->nombres = $request->nombres ?? "";
        $roleData->apellido_paterno = $request->apellido_paterno ??  $request->apellidoPaterno ?? "";
        $roleData->apellido_materno = $request->apellido_materno ??   $request->apellidoMaterno ?? "";
        $roleData->email = $request->email ?? "";
        $roleData->remember_token = $request->remember_token ?? "";
        $roleData->telefono = $request->telefono ?? "";
    }
}
