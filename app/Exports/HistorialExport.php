<?php

namespace App\Exports;

use App\Models\Historial;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class HistorialExport implements FromCollection, WithHeadings
{
    private $startDate = '';
    private $endDate = '';
    public function setStartDate(string $date)
    {
        $this->startDate = $date;
    }
    public function setEndDate(string $date)
    {
        $this->endDate = $date;
    }

    public function collection()
    {
        $historial = new Historial();
        if ($this->startDate != '' && $this->endDate != '') {
            if ($this->startDate == $this->endDate)
                $response = $historial->whereDate('created_at', $this->startDate);
            if ($this->startDate != $this->endDate)
                $response = $historial->whereBetween('created_at', [$this->startDate, $this->endDate]);
            $response = $response->get(['ficha_id', 'rut_ciudadano', 'nombre_ciudadano', 'nombre_asesor', 'rut_asesor', 'ley_consultada', 'lugar', 'created_at',]);
            $response = $response->map(function ($row) {
                $row['dia'] = $row->created_at->format('d');
                $row['mes'] = $row->created_at->format('M');
                $row['año'] = $row->created_at->format('Y');
                return $row;
            });
        }
        if ($this->startDate == '' && $this->endDate == '') {
            $response = $historial->all();
        }
        return $response;
    }

    public function headings(): array
    {
        return ['ficha id', 'rut ciudadano', 'nombre ciudadano', 'nombre asesor', 'rut asesor', 'ley consultada', 'lugar', 'fecha consultada', 'dia', 'mes', 'año'];
    }
}
