<?php

namespace App\Http\Controllers;


use App\Http\Controllers\Controller;


class AuthController extends Controller
{

    public function login()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth('web')->attempt($credentials)) {
            return $this->responseFail(['message' => 'Email o Contraseña incorrecta. Vuelve a intentarlo']);
        }
        $user = auth('web')->user();
        if (!$user->email_verified_at) {
            auth('web')->logout();
            return $this->responseFail(['message' => 'El email no esta validado']);
        }
        if ($user->is_blocked) {
            auth('web')->logout();
            return $this->responseFail(['message' => 'El usuario esta bloqueado comuniquese con admin@emprendepo.cl']);
        }
        return $this->respondWithToken(['token' => $token, 'roles' => $user->roles->pluck('nombre')]);
    }

    public function loginPublic()
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth('public')->attempt($credentials)) {
            return $this->responseFail(['message' => 'Email o Contraseña incorrecta. Vuelve a intentarlo']);
        }
        $user = auth('public')->user();
        if (!$user->email_verified_at) {
            auth('public')->logout();
            return $this->responseFail(['message' => 'El email no esta validado']);
        }
        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth('web')->user();
        $auth['name'] = $user->name;
        $auth['email'] = $user->email;
        $auth['empresa'] = $user->empresas;
        $auth['suscripciones'] = $user->empresas->Suscripciones;
        $auth['roles'] = $user->roles->pluck('nombre');
        return $this->responseSuccess($auth);
    }

    public function mePublic()
    {
        $user = auth('public')->user();
        $auth = $user;
        $auth['direcciones'] = $user->direcciones;
        return $this->responseSuccess($auth);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('web')->logout();
        return $this->responseSuccess(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth('web')->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return $this->responseSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('web')->factory()->getTTL() * 60
        ]);
    }
}
