<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Hash;
use App\Models\{Usuarios, DireccionesUsuarios, PortalBoletas};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Mail};
use App\Mail\{userMail};
use Carbon\Carbon;

class UserPortalController extends Controller
{

    public function me(Request $request)
    {
        $user = auth('public')->user();
        return $this->responseSuccess($user);
    }

    public function meDirecciones(Request $request)
    {
        $user = auth('public')->user();
        return $this->responseSuccess($user->direcciones);
    }

    public function addDireccion(Request $request)
    {
        $user = auth('public')->user();
        $direccion = new DireccionesUsuarios;
        $direccion->descripcion = $request->descripcion;
        $direccion->calle = $request->direccion;
        $direccion->comuna = $request->comuna;
        $direccion->region = $request->region;
        $direccion->usuario_id = $user->id;
        $direccion->save();
        return $this->responseSuccess($direccion);
    }



    public function store(Request $request)
    {
        $validateNombre = Usuarios::whereRaw('LOWER(email) = ?', [trim(strtolower($request->email))])->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El email del usuario ya existe, intente nuevamente.']);
        }
        $this->responseSuccess($request->nombres);
        $hash  =  Hash::make($request->email);
        $request->merge(['entidad' => 'Usuarios', 'remember_token' => $hash]);
        $genericController =  new GenericController($request, true);
        $usuario = $genericController->store($request);
        $this->handleMail($request);
        return $this->responseSuccess(['id' => $usuario]);
    }

    public function update(Request $request)
    {
        $email = $request->email ?? null;
        if (!$email) {
            return $this->responseFail(['message' => 'El email es requerido.']);
        }
        $user = auth('public')->user();
        if ($user->email !== $email) {
            $usaurio_mail = Usuarios::where('email', $email)->first();
            if ($usaurio_mail)
                return $this->responseFail(['message' => 'El email ya se encuentra registrado en nuestra base de datos.']);
        }
        $request->merge(['entidad' => 'Usuarios']);
        $genericController =  new GenericController($request, true);
        $Usuarios = $genericController->update($request, "Usuarios", $user->id);
        return $this->responseSuccess($Usuarios);

        return $this->responseSuccess(['id' => $user]);
    }

    public function validateToken(Request $request)
    {
        if (!$request->token) {
            return $this->responseFail(['message' => 'Se requiere un Token']);
        }
        $user = Usuarios::where('remember_token', $request->token)->first();
        if (!$user) {
            return $this->responseFail(['message' => 'El token no es valido.']);
        }
        $user->password = Hash::make($request->password);
        $user->remember_token = null;
        $user->email_verified_at = Carbon::now();
        $user->save();
        $credentials = ['email' => $user->email, 'password' => $request->password];
        if (!$token = auth('public')->attempt($credentials)) {
            return $this->responseFail(['message' => 'Email o Contraseña incorrecta. Vuelve a intentarlo']);
        }
        $user = auth('public')->user();
        return $this->respondWithToken($token);
    }

    protected function handleMail($data)
    {
        Mail::to($data->email)->send(new userMail("Valida y Crear password", [
            'name' => $data->nombres,
            'url' => 'http://localhost:3001/change/password?token=' . $data->remember_token,
        ]));
    }

    public function deleteDireccion($id)
    {
        $pedido = DireccionesUsuarios::findOrFail($id);
        $pedido->delete();
        return $this->responseSuccess($pedido);
    }

    public function getCompras()
    {
        $user = auth('public')->user();
        $boletas = PortalBoletas::with(['Compras.Direccion', 'Compras.Detalle.Productos.Files', 'Compras.Detalle.Productos.Categoria'])->where('usuario_id', $user->id)->orderBy('id', 'desc')->get();
        return $this->responseSuccess($boletas);
    }

    protected function respondWithToken($token)
    {
        return $this->responseSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('web')->factory()->getTTL() * 60
        ]);
    }
}
