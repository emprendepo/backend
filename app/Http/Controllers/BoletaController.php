<?php

namespace App\Http\Controllers;

use App\Models\{Productos, Clientes, MetodoDePago, Boletas, DetalleVentas, Ventas};
use Illuminate\Http\Request;

class BoletaController extends Controller
{
    public function index(Request $request)
    {
        $form  = [];
        $formDetalle = [];
        $productos = Productos::get(['id as value', 'descripcion as label']);
        $metodoDePago = MetodoDePago::get(['id as value', 'nombre as label']);
        $clientes = Clientes::get(['id as value', 'nombres as label']);
        $form[0]['name'] = 'productos';
        $form[0]['label'] = 'Productos';
        $form[0]['inputRequired'] = true;
        $form[0]['inputType'] = 'Select';
        $form[0]['items'] = $productos;
        $form[1]['name'] = 'cantidad';
        $form[1]['label'] = 'Cantidad de productos';
        $form[1]['inputRequired'] = true;
        $form[1]['inputType'] = 'TextInput';
        $form[1]['inputType'] = 'TextInput';
        $form[1]['type'] = 'Number';
        $form[2]['name'] = 'valor';
        $form[2]['label'] = 'Valor del productos';
        $form[2]['inputRequired'] = true;
        $form[2]['inputType'] = 'TextInput';
        $form[2]['inputType'] = 'TextInput';
        $form[2]['type'] = 'Number';

        $formDetalle[0]['name'] = 'cliente';
        $formDetalle[0]['label'] = 'Clientes';
        $formDetalle[0]['inputRequired'] = true;
        $formDetalle[0]['inputType'] = 'Select';
        $formDetalle[0]['items'] = $clientes;

        $formDetalle[2]['name'] = 'metodoDePago';
        $formDetalle[2]['label'] = 'Metodo de Pago';
        $formDetalle[2]['inputRequired'] = true;
        $formDetalle[2]['inputType'] = 'Select';
        $formDetalle[2]['items'] = $metodoDePago;

        $formDetalle[3]['name'] = 'fechaBoleta';
        $formDetalle[3]['label'] = 'Fecha Boleta';
        $formDetalle[3]['inputRequired'] = true;
        $formDetalle[3]['inputType'] = 'TextInput';
        $formDetalle[3]['type'] = 'Date';

        return $this->responseSuccess(['form' => $form, 'formDetalle' => $formDetalle, 'productos' => $productos, 'metodosDePago' => $metodoDePago, 'clientes' => $clientes]);
    }

    public function store(Request $request)
    {
        $newBoleta = new Boletas();
        $valores = $this->Valores($request);
        $newBoleta->folio = $request->has('folio') ? $request->folio : $this->getFolio();
        $newBoleta->precio_neto = $valores['precio_neto'];
        $newBoleta->precio_con_iva = $valores['precio_con_iva'];
        $newBoleta->costo_iva = $valores['costo_iva'];
        $newBoleta->fecha_de_venta = isset($request->detalle['fechaBoleta']) ? $request->detalle['fechaBoleta'] : 0;
        $newBoleta->clientes_id = isset($request->detalle['clienteID']) ? $request->detalle['clienteID'] : 0;
        $newBoleta->metodo_de_pago_id = isset($request->detalle['metodoPagoId']) ? $request->detalle['metodoPagoId'] : 0;
        $newBoleta->save();
        $id = $newBoleta->id;
        $ventas = new Ventas();
        $ventas->boletas_id = $id;
        $ventas->fecha_creacion = isset($request->detalle['fechaBoleta']) ? $request->detalle['fechaBoleta'] : 0;
        $ventas->save();
        $idVentas = $ventas->id;
        collect($request->items)->each(function ($item, $key) use ($idVentas, $request) {
            $detalleVentas = new DetalleVentas();
            $detalleVentas->ventas_id = $idVentas;
            $detalleVentas->productos_id  = $item['idProducto'];
            $detalleVentas->cantidad  = intVal($item['cantidad']);
            $detalleVentas->valor  = intVal($item['valor']);
            $detalleVentas->save();
        });
        return $this->responseSuccess(['id' => $idVentas]);
    }

    public static function Valores($request)
    {
        $precio_con_iva = 0;
        $costo_iva = 0;
        $newValores = collect($request->items)->sum('valor');
        $iva = round($newValores * 1.19);
        return ['precio_neto' => $newValores, 'precio_con_iva' => $iva, 'costo_iva' => round($iva - $newValores)];
    }


    public static function getFolio()
    {
        $factura = Boletas::latest()->first(['folio']);
        if ($factura) {
            return ++$factura->folio;
        }
        return '1';
    }

    public function getID($id)
    {
        $factura = Boletas::with(['Clientes:id,nombres', 'metododepago:id,nombre', 'Ventas.DetalleVentas.Productos'])->where('folio', $id)->first();
        // if ($factura) {
        //     $data['detalle']['folio'] = $factura->folio;
        //     $data['detalle']['clienteName'] = $factura->clientes['nombres'];
        //     $data['detalle']['trabajadorName'] = [];
        //     $data['detalle']['metodoName'] = $factura->metododepago['nombre'];
        //     $data['detalle']['fechaBoleta'] = $factura->fecha_de_venta;
        //     $data['items'] = collect($factura['ventas']['detalleventas']);
        // }

        return $this->responseSuccess($factura);
    }
}
