<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Transbank\Webpay\WebpayPlus\Transaction;
use App\Models\{CarroCompra, PortalBoletas, PortalCompras, PortalComprasDetalle, Clientes, Usuarios, Boletas, Ventas, DetalleVentas, Pedidos};

class PagosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    public function result(Request $request)
    {
        $transaction = new Transaction();
        $token = $request->token_ws;
        $response = $transaction->commit($token);
        if ($response->isApproved()) {
            $carro = CarroCompra::find($response->buyOrder);
            $carro->result = json_encode($response);
            $carro->token = $token;
            $carro->save();
            $json = json_decode($carro->data);
            $portalBoletas = PortalBoletas::updateOrCreate(['folio' => $carro->id], ["monto_total" => $json->products->valor_total, "cantidades" => $json->products->cantidad_total, "fecha_de_compra" =>  $carro->created_at, "usuario_id" => $carro->usuario_id]);
            $portalCompras = PortalCompras::updateOrCreate(["portal_boletas_id" => $portalBoletas->id], ["portal_boletas_id" => $portalBoletas->id, "direccion_id" => $json->state->value]);
            $productos = collect($json->products->detalle);
            $productos->each(function ($items) use ($portalCompras) {
                PortalComprasDetalle::updateOrCreate(['portal_compras_id' => $portalCompras->id, 'emp_productos_id' => $items->id], ['portal_compras_id' => $portalCompras->id, 'emp_productos_id' => $items->id, 'cantidad' => $items->cantidad, 'valor' => $items->valor]);
            });
            $usuario  = Usuarios::find($carro->usuario_id);
            $productos->groupBy('empresa.id')->each(function ($producto, $key) use ($usuario, $carro, $json) {
                $calle = $json->state->data->calle;
                $comuna = $json->state->data->comuna;
                $region = $json->state->data->region;
                $direccion = "$calle , $comuna , $region .";
                $Clientes = Clientes::updateOrCreate(["rut" => $usuario->rut . $usuario->dv, 'empresas_id' => $key], ['rut' => $usuario->rut . $usuario->dv, 'nombres' => $usuario->nombres, 'apellido_paterno' => $usuario->apellido_paterno, 'apellido_materno' => $usuario->apellido_materno, 'email' => $usuario->email, 'telefono' => $usuario->telefono, 'direccion' => $direccion]);
                $newValores = $producto->sum('valor_total');
                $cantidades = $producto->sum('cantidad');
                $iva = round($newValores / 1.19);
                $boletasVenta = Boletas::updateOrCreate(['folio' => $carro->id, 'clientes_id' => $Clientes->id, 'empresas_id' => $key], ['folio' => $carro->id, 'clientes_id' => $Clientes->id, 'empresas_id' => $key, 'fecha_de_venta' => $carro->created_at, 'metodo_de_pago_id' => 1, 'precio_neto' => $iva, 'precio_con_iva' => $newValores, 'costo_iva' => round($newValores - $iva)]);
                $venta = Ventas::updateOrCreate(['boletas_id' => $boletasVenta->id, 'empresas_id' => $key], ['boletas_id' => $boletasVenta->id, 'empresas_id' => $key, 'fecha_creacion' => $boletasVenta->created_at]);
                collect($producto)->each(function ($items) use ($venta) {
                    DetalleVentas::updateOrCreate(['ventas_id' => $venta->id, 'productos_id' => $items->id], ['ventas_id' => $venta->id, 'productos_id' => $items->id, 'cantidad' => $items->cantidad, 'valor' => $items->valor]);
                });

                Pedidos::updateOrCreate(['boletas_id' => $boletasVenta->id, 'empresas_id' => $key], ['boletas_id' => $boletasVenta->id, 'empresas_id' => $key, 'cantidades' => $cantidades, 'valor_total' => $newValores, 'direccion' => $direccion]);
            });
            $carro->delete();
            return redirect('http://localhost:3001/config/usuario');
        } else {
            return $this->responseFail($response);
        }
        return $this->responseSuccess([$token]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $user = auth('public')->user();
        $carro = CarroCompra::where('uuid', $request->uuid)->first();
        $carro->data = $request->data;
        $carro->usuario_id = $user->id;
        $carro->save();
        $transaction = new Transaction();
        $response = $transaction->create($carro->id, uniqid(), $request->total, env('BASE_URL', 'https://api.emprendepo.test/') . 'api/public/transaction/result');
        return $this->responseSuccess($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
