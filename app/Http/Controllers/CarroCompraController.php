<?php

namespace App\Http\Controllers;

use App\Models\{CarroCompra, CarroCompraDetalle};
use Illuminate\Http\Request;

class CarroCompraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $arrayCarro = ["uuid" => $request->uuid, "usuario_id" => $request->usuarioId];
        $carro = CarroCompra::updateOrCreate($arrayCarro, $arrayCarro);
        $arrayCarroDetalle = ["carros_de_compras_id" => $carro->id, "emp_productos_id" => $request->productoId];
        $detalleCarro = CarroCompraDetalle::updateOrCreate($arrayCarroDetalle, $arrayCarroDetalle);
        $detalleCarro->cantidad = $request->cantidad ?? $detalleCarro->cantidad + 1;
        $detalleCarro->save();
        return $this->responseSuccess($detalleCarro);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $data = CarroCompra::with(["CarroCompraDetalle.Productos.empresas", "CarroCompraDetalle.Productos.Files"])->where("uuid", $request->uuid)->first();
        if ($data) {
            $detalle =  $this->formatDetalle($data->CarroCompraDetalle);
            $item["carroID"] = $data->id;
            $item["uuid"] = $request->uuid;
            $item["userID"] = $request->usuario_id;
            $item["cantidad_total"] = $detalle->sum('cantidad');
            $item["valor_total"] = $detalle->sum('valor_total');
            $item["detalle"] = $detalle;
            $data = $item;
        }

        return $this->responseSuccess($data);
    }

    protected function formatDetalle($carros)
    {
        return $carros->map(function ($carro) {
            $producto = $carro->productos;
            $cantidad = $carro->cantidad;
            $img = isset($carro->productos->files) && isset($carro->productos->files[0]) ? $carro->productos->files[0] : false;
            $item["id"] = $producto->id;
            $item["nombre"] = $producto->nombre;
            $item["descripcion"] = $producto->descripcion;
            $item["valor"] = $producto->valor;
            $item["valor_total"] = round($producto->valor * $cantidad);
            $item["cantidad"] = $cantidad;
            $item["empresa"]["id"] = $producto->empresas->id;
            $item["empresa"]["name"] = $producto->empresas->name;
            if ($img) {
                $item["img"]["name"] = $img->name;
                $item["img"]["path"] = $img->path;
            }
            return $item;
        });
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $carroDetalle = CarroCompraDetalle::where(['carros_de_compras_id' => $request->carroID, 'emp_productos_id' => $request->productoId])->first();
        if ($carroDetalle) {
            $carroDetalle->delete();
            return $this->responseSuccess($carroDetalle);
        }
        return $this->responseSuccess("error");
    }
}
