<?php

namespace App\Http\Controllers;

use Transbank\Webpay\Oneclick\MallInscription;
use App\Models\{Suscripciones, Empresas};
use Illuminate\Http\Request;

class SuscripcionesController extends Controller
{

    public function index()
    {
    }

    public function validateSuscripcion(Request $request)
    {
        $user = auth('web')->user();
        $empresa = $user->empresas;
        $username = $empresa->name ?? null;
        $email = $empresa->email ?? null;
        if (!$email) {
            return $this->responseFail(['message' => 'La empresa no tiene un email asignado.']);
        }
        $response_url = env('BASE_URL', 'https://api.emprendepo.test/') . "api/public/suscripcion/result";
        $response = (new MallInscription)->start($username, $email, $response_url);
        $empresa->token_inscripcion = $response->token;
        $empresa->type = $request->type;
        $empresa->save();
        return $this->responseSuccess(['data' => $response]);
    }

    public function result()
    {
        $suscripcion = new Suscripciones;
        $tbk_token = $_GET['TBK_TOKEN'];
        $inscripcion = new MallInscription;
        $response = $inscripcion->finish($tbk_token);
        $empresa = Empresas::where('token_inscripcion', $tbk_token)->first();
        $suscripcion->response_code = $response->responseCode;
        $suscripcion->tbk_user = $response->tbkUser;
        $suscripcion->authorization_code = $response->authorizationCode;
        $suscripcion->card_type = $response->cardType;
        $suscripcion->type = $empresa->type;
        $suscripcion->valor = $empresa->type == "1" ? 19990 : 22990;
        $suscripcion->card_number = $response->cardNumber;
        $suscripcion->empresas_id = $empresa->id;
        $suscripcion->save();
        return redirect('http://localhost:3000/pyme/configuracion');
    }

    public function delete()
    {
        $user = auth('web')->user();
        $empresa = $user->empresas;
        $suscripcion = $empresa->Suscripciones;
        $response = (new MallInscription)->delete($suscripcion->tbk_user, $empresa->name);
        $suscripcion->delete();
        return $this->responseSuccess(['data' => $response]);
    }
}
