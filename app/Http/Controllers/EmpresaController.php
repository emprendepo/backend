<?php

namespace App\Http\Controllers;

use App\Exports\{GenericExport};
use Maatwebsite\Excel\Facades\Excel;
use App\Models\{Categoria, Clientes, Empresas, Ventas, Productos};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\{Mail};
use App\Mail\{userMail};
use Carbon\Carbon;

class EmpresaController extends Controller
{

    public function index()
    {
        $user = auth('web')->user();
        $empresaID = $user->empresas->id;
        $ventas = Ventas::with(['Boletas.metodoDePago', 'DetalleVentas.Productos'])->where('empresas_id', $empresaID)->get();
        if (!count($ventas)) {
            return $this->responseSuccess(['view' => 0]);
        }
        $donuts = $ventas->map(function ($item, $key) {
            return $item->Boletas->metodoDePago;
        })->groupBy('nombre');

        $dataDonuts['labels'] = $donuts->keys();
        $dataDonuts['series'] = $donuts->map(function ($item) {
            return count($item);
        })->values();

        $dataBasicBar = $ventas->map(function ($item, $key) {
            $data['fecha'] = $item->fecha_creacion;
            $data['total'] = $item->Boletas->precio_con_iva;
            return $data;
        })->groupBy('fecha');

        $responseBasicBar['categories'] = $dataBasicBar->keys();
        $responseBasicBar['data'] = $dataBasicBar->map(function ($item) {
            return $item->sum('total');
        })->filter()->values();

        //Producto mas vendido.
        $dataProducto = $ventas->map(function ($item, $key) {
            return $item->DetalleVentas->map(function ($item, $key) {
                $data['producto'] = $item->productos_id;
                $data['cantidad'] = $item->cantidad;
                return $data;
            })->groupBy('producto')->collapse();
        })->collapse();

        $producto = $dataProducto->groupBy('producto')->map(function ($item, $key) {
            $data['id'] = $key;
            $data['cantidad'] = $item->sum('cantidad');
            return $data;
        })->sortByDesc('cantidad')->first();

        $responseProducto = Productos::find($producto['id']);
        //Categoria maas vendida
        $dataCategoria = $ventas->map(function ($item, $key) {
            return $item->DetalleVentas->map(function ($item, $key) {
                $data['categoria'] = $item->Productos->categoria_id;
                return $data;
            })->groupBy('categoria')->collapse();
        })->collapse();


        $categoria = $dataCategoria->groupBy('categoria')->map(function ($item, $key) {
            $data['id'] = $key;
            $data['cantidad'] = count($item);
            return $data;
        })->sortByDesc('cantidad')->first();

        $responseCategoria = Categoria::find($categoria['id']);

        $responseClientes = Clientes::where('empresas_id', $empresaID)->get()->take(10);

        return $this->responseSuccess(['view' => 1, 'donuts' => $dataDonuts, 'BasicBar' => $responseBasicBar, 'masVendidoProducto' => $responseProducto, 'masVendidoCategoria' => $responseCategoria, 'clientes' => $responseClientes]);
    }

    public function store(Request $request)
    {
        $validateNombre = Empresas::whereRaw('LOWER(name) = ?', [trim(strtolower($request->name))])->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El nombre de la empresa ya existe, intente nuevamente.']);
        }
        $validateNombre = Empresas::orWhere('rut', $request->rut)->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El rut de la empresa ya existe, intente nuevamente.']);
        }
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $empresa = $genericController->store($request);
        $this->responseSuccess(['id' => $empresa]);
    }

    public function storePublic(Request $request)
    {
        $validateNombre = Empresas::whereRaw('LOWER(name) = ?', [trim(strtolower($request->name))])->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El nombre de la empresa ya existe, intente nuevamente.']);
        }
        $validateNombre = Empresas::orWhere('rut', $request->rut)->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El rut de la empresa ya existe, intente nuevamente.']);
        }
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $empresa = $genericController->store($request);
        $hash  =  Hash::make($request->email);
        $request->merge(['entidad' => 'user', 'remember_token' => $hash, 'empresas_id' => $empresa]);
        $genericController =  new GenericController($request, true);
        $usuario = $genericController->store($request);
        $this->handleMail($request);
        $this->responseSuccess(['id' => $usuario]);
    }

    protected function handleMail($data)
    {
        Mail::to($data->email)->send(new userMail("Valida y Crear password", [
            'name' => $data->name,
            'url' => 'http://localhost:3000/change/password?token=' . $data->remember_token,
        ]));
    }

    public function informes(Request $request)
    {
        $user = auth('web')->user();
        $empresaID = $user->empresas->id;
        if ($request->informe === "Ventas") {
            return $this->informeVentas($request, $empresaID);
        }

        if ($request->informe === "Productos") {
            return $this->informeProductos($request, $empresaID);
        }

        if ($request->informe === "Clientes") {
            return $this->informeClientes($request, $empresaID);
        }

        return $this->responseFail("No Existe el informe.");
    }

    protected function Dates($request)
    {
        $fecha = new Carbon();
        $initialDate = $request->startDate ? (new Carbon($request->startDate))->startOfDay() : $fecha->now()->startOfDay();
        $finalDate = $request->endDate ? (new Carbon($request->endDate))->endOfDay() : $fecha->endOfDay();
        return ['initialDate' => $initialDate, 'finalDate' => $finalDate];
    }


    protected function informeVentas($request, $empresaID)
    {
        $fecha = $this->Dates($request);
        $ventas = Ventas::with(['Boletas.metodoDePago', 'DetalleVentas.Productos'])->whereBetween('created_at', [$fecha['initialDate'], $fecha['finalDate']])->where('empresas_id', $empresaID)->get();
        $response = $ventas->map(function ($item) {
            $data['id'] = $item->id ?? "";
            $data['boleta_folio'] = $item->Boletas->folio ?? "";
            $data['precio_neto'] = $item->Boletas->precio_neto ?? "";
            $data['precio_con_iva'] = $item->Boletas->precio_con_iva ?? "";
            $data['costo_iva'] = $item->Boletas->costo_iva ?? "";
            $data['fecha'] = $item->created_at ?? "";
            $data['medio_de_pago'] = $item->Boletas->metodoDePago->nombre ?? "";
            return $data;
        })->all();
        if ($request->export) {
            $colums = ['id', 'boleta_folio', 'precio_neto', 'precio_con_iva', 'costo_iva', 'fecha', 'medio_de_pago'];
            return Excel::download(new GenericExport(collect($response), $colums), 'repo_fuga' . '-' . time() . '-' . '.xlsx')->deleteFileAfterSend(false);
        }
        return $this->responseSuccess($response);
    }


    protected function informeProductos($request, $empresaID)
    {
        $fecha = $this->Dates($request);
        $ventas = Ventas::with(['Boletas.metodoDePago', 'DetalleVentas.Productos'])->whereBetween('created_at', [$fecha['initialDate'], $fecha['finalDate']])->where('empresas_id', $empresaID)->get();
        $response = $ventas->map(function ($item) {
            $id = $item->id;
            $folio = $item->Boletas->folio;
            return $item->DetalleVentas->map(function ($item, $key) use ($id, $folio) {
                $data['id'] = $id;
                $data['folio'] = $folio;
                $data['nombre'] = $item->productos->nombre ?? "";
                $data['numero_serie'] = $item->productos->numero_serie ?? "";
                $data['cantidad'] = $item->cantidad ?? "";
                return $data;
            });
        })->collapse();
        if ($request->export) {
            $colums = ['id', 'boleta_folio', 'nombre', 'numero_serie', 'cantidad'];
            return Excel::download(new GenericExport(collect($response), $colums), 'repo_fuga' . '-' . time() . '-' . '.xlsx')->deleteFileAfterSend(false);
        }
        return $this->responseSuccess($response);
    }

    protected function informeClientes($request, $empresaID)
    {
        $fecha = $this->Dates($request);
        $response = Clientes::whereBetween('created_at', [$fecha['initialDate'], $fecha['finalDate']])->where('empresas_id', $empresaID)->get();
        if ($request->export) {
            $colums = ['id', 'rut', 'nombres', 'apellido_paterno', 'apellido_materno', 'direccion', 'telefono', 'email', 'fecha_nacimiento'];
            return Excel::download(new GenericExport(collect($response), $colums), 'repo_fuga' . '-' . time() . '-' . '.xlsx')->deleteFileAfterSend(false);
        }
        return $this->responseSuccess($response);
    }


    public function list(Request $request)
    {
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $faqList = $genericController->index($request);
        return $this->responseSuccess($faqList);
    }

    public function form(Request $request)
    {
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $faqList = $genericController->form($request);
        return $this->responseSuccess($faqList);
    }



    public function show($id)
    {
        $empresa = Empresas::with(["Productos.empresas", "Productos.Files"])->findOrFail($id);
        return $this->responseSuccess($empresa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function updateForm(Request $request, $id)
    {
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $empresasList = $genericController->update($request, 'empresas', $id);
        return $this->responseSuccess($empresasList);
    }

    public function update(Request $request, $id)
    {
        $empresa = Empresas::findOrFail($id);
        $empresa->fill($request->all())->save();
        return $this->responseSuccess($empresa);
    }


    public function destroy(Request $request, $id)
    {
        $request->merge(['entidad' => 'empresas']);
        $genericController =  new GenericController($request, true);
        $empresasList = $genericController->destroy($request, $id);
        return $this->responseSuccess($empresasList);
    }
}
