<?php

namespace App\Http\Controllers;

use App\Models\{User};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\{Mail};
use App\Mail\{userMail};
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        //
    }

    public function store(Request $request)
    {
        $validateNombre = User::whereRaw('LOWER(email) = ?', [trim(strtolower($request->email))])->first();
        if ($validateNombre) {
            return $this->responseFail(['message' => 'El email del usuario ya existe, intente nuevamente.']);
        }
        $hash  =  Hash::make($request->email);
        $request->merge(['entidad' => 'user', 'remember_token' => $hash]);
        $genericController =  new GenericController($request, true);
        $empresa = $genericController->store($request);
        $this->handleMail($request);
        $this->responseSuccess(['id' => $empresa]);
    }

    public function testMail(Request $request)
    {
        $this->handleMail($request);
        return $this->responseSuccess(['id' => $request->remember_token]);
    }

    public function validateToken(Request $request)
    {
        if (!$request->token) {
            return $this->responseFail(['message' => 'Se requiere un Token']);
        }
        $user = User::where('remember_token', $request->token)->first();
        if (!$user) {
            return $this->responseFail(['message' => 'El token no es valido.']);
        }
        $user->password = Hash::make($request->password);
        $user->remember_token = null;
        $user->email_verified_at = Carbon::now();
        $user->save();
        $user->roles()->sync([1]);
        $credentials = ['email' => $user->email, 'password' => $request->password];
        if (!$token = auth('web')->attempt($credentials)) {
            return $this->responseFail(['message' => 'Email o Contraseña incorrecta. Vuelve a intentarlo']);
        }
        $user = auth('web')->user();
        return $this->respondWithToken(['token' => $token, 'roles' => $user->roles->pluck('nombre')]);
    }

    public function validateMail(Request $request)
    {
        if (!$request->email) {
            return $this->responseFail(['message' => 'Se requiere un email']);
        }
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return $this->responseFail(['message' => 'El email no se encuentra registrado en nuestra base de datos.']);
        }
        $hash = Hash::make($request->email);
        $user->password = null;
        $user->remember_token = $hash;
        $user->email_verified_at = null;
        $user->save();
        $request->merge(['name' => $user->name, 'email' => $request->email, 'remember_token' => $hash]);
        $this->handleMail($request);
        return $this->responseSuccess($user->email);
    }

    protected function handleMail($data)
    {
        Mail::to($data->email)->send(new userMail("Valida y Crear password", [
            'name' => $data->name,
            'url' => 'http://localhost:3000/change/password?token=' . $data->remember_token,
        ]));
    }

    protected function respondWithToken($token)
    {
        return $this->responseSuccess([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('web')->factory()->getTTL() * 60
        ]);
    }


    public function list(Request $request)
    {
        $request->merge(['entidad' => 'user']);
        $genericController =  new GenericController($request, true);
        $faqList = $genericController->index($request);
        return $this->responseSuccess($faqList);
    }

    public function form(Request $request)
    {
        $request->merge(['entidad' => 'User']);
        $genericController =  new GenericController($request, true);
        $faqList = $genericController->form($request);
        return $this->responseSuccess($faqList);
    }



    public function show($id)
    {
        $empresa = User::with(["Productos.empresas", "Productos.Files"])->findOrFail($id);
        return $this->responseSuccess($empresa);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function updateForm(Request $request, $id)
    {
        $request->merge(['entidad' => 'user']);
        $genericController =  new GenericController($request, true);
        $empresasList = $genericController->update($request, "user", $id);
        return $this->responseSuccess($empresasList);
    }

    public function update(Request $request)
    {
        $email = $request->email ?? null;
        if (!$email) {
            return $this->responseFail(['message' => 'El email es requerido.']);
        }
        $user = auth('web')->user();
        if ($user->email !== $email) {
            $usaurio_mail = User::where('email', $email)->first();
            if ($usaurio_mail)
                return $this->responseFail(['message' => 'El email ya se encuentra registrado en nuestra base de datos.']);
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->update();
        return $this->responseSuccess($user);
    }


    public function destroy(Request $request, $id)
    {
        $request->merge(['entidad' => 'user']);
        $genericController =  new GenericController($request, true);
        $empresasList = $genericController->destroy($request, $id);
        return $this->responseSuccess($empresasList);
    }
}
