<?php

namespace App\Http\Controllers;

use App\Models\{Pedidos, Empresas};
use Illuminate\Http\Request;


class PedidosController extends Controller
{
    public function update(Request $request, $id)
    {
        $pedido = Pedidos::findOrFail($id);
        $pedido->estado = $request->estado;
        $pedido->save();
        return $this->responseSuccess($pedido);
    }

    public function generateEnvio($id)
    {
        $pedido = Pedidos::with(['Boletas.Clientes'])->find($id);
        $empresa =  Empresas::find($pedido->empresas_id);
        $logo = $empresa->logo ? str_replace("public/", "", $empresa->logo ??  "/public") : "/public";
        return view('pedidos.despacho', ['nombre_cliente' => $pedido->Boletas->Clientes->nombres, 'rut' => $pedido->Boletas->Clientes->rut, 'email' => $pedido->Boletas->Clientes->email, 'direccion' => $pedido->direccion, 'logo' => $logo, 'url' => $empresa->sitio]);
    }
}
