<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function responseSuccess($data = [], $code = 200)
    {
        return response()
            ->json([
                'code' => $code,
                'success' => true,
                'data' => $data
            ], $code);
    }

    /**
     * Formatted failed response helper
     *
     * @param array $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail($data = [], $code = 400)
    {
        return response()
            ->json([
                'code' => $code,
                'success' => false,
                'data' => $data
            ], $code);
    }

    /**
     * Override validation
     *
     * @param array $data
     * @param int $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function validateFunction(Request $request, array $rules, array $messages = [], array $customAttributes = [])
    {
        $validator = \Validator::make($request->all(), $rules, $messages, $customAttributes);
        if ($validator->fails()) {
            throw new ValidationException($validator, $this->responseFail(['message' => $validator->messages()->unique()]));
        }
    }
}
