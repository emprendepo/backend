<?php

namespace App\Utils;

class Rut
{
    /**
     * Limpia el rut dejando solo numeros y 'k'
     */
    public static function cleanRut($rut)
    {
        return preg_replace('/[^k0-9]/i', '', $rut);
    }

    public static function explodeRut($rut)
    {
        if (strpos($rut, '-') !== false) {
            return explode('-', $rut);
        }
    }

    /**
     * valida si el rut es valido
     */
    public static function validateRut($rut)
    {

        $rut = self::formatRut(self::cleanRut($rut));
        if (!preg_match("/^[0-9.]+-[0-9kK]{1}/", $rut)) {
            return false;
        }
        $rut = explode('-', $rut);
        return strtolower($rut[1]) == strtolower(self::calcDv($rut[0]));
    }
    /**
     * Formatea el rut con '.' y '-'
     */
    public static function formatRut($rut)
    {
        if (strpos($rut, '-') !== false) {
            $rut = implode('', explode('-', $rut));
        }
        return number_format(substr($rut, 0, -1), 0, "", ".") . '-' . substr($rut, strlen($rut) - 1, 1);
    }

    /**
     * Calcula el dv
     */
    public static function calcDv($rut)
    {
        $rut = self::cleanRut($rut);
        $s = 1;
        for ($m = 0; $rut != 0; $rut /= 10) {
            $s = ($s + $rut % 10 * (9 - $m++ % 6)) % 11;
        }
        return chr($s ? $s + 47 : 75);
    }

    /**
     * return rut sin 'dv', '.' y '-'
     */
    public static function servicioRut($rut)
    {
        $rut = substr($rut, 0, strlen($rut) - 1);
        $rut = self::cleanRut($rut);
        return $rut;
    }

    /**
     * return rut con dv y '-'
     */
    public static function formatRutWS($rut)
    {
        $dv = self::calcDv($rut);
        return self::cleanRut($rut) . '-' . $dv;
    }
}
