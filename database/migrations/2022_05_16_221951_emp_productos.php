<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('emp_productos', function (Blueprint $table) {
            $table->id();
            $table->string('descripcion');
            $table->string('numero_serie');
            $table->string('valor');
            $table->date('fecha_vencimiento');
            $table->unsignedBigInteger('empresas_id');
            $table->foreign('empresas_id')->references('id')->on('emp_empresas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('emp_productos');
    }
};
