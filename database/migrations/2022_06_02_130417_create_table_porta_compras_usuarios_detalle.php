<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_compras_usuarios_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portal_compras_id')->nullable();
            $table->foreign('portal_compras_id')->references('id')->on('portal_compras_usuarios');
            $table->unsignedBigInteger('emp_productos_id');
            $table->foreign('emp_productos_id')->references('id')->on('emp_productos');
            $table->integer('cantidad')->default(0);
            $table->integer('valor')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_compras_usuarios_detalle');
    }
};
