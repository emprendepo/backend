<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('emp_ventas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('boletas_id')->unique();
            $table->foreign('boletas_id')->references('id')->on('emp_boletas');
            $table->date('fecha_creacion');
            $table->unsignedBigInteger('empresas_id');
            $table->foreign('empresas_id')->references('id')->on('emp_empresas');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('emp_ventas');
    }
};
