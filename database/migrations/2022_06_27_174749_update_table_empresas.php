<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('emp_empresas', function ($table) {
            $table->string('logo')->nullable()->after('descripcion');
            $table->string('fondo')->nullable()->after('logo');
            $table->string('facebook')->nullable()->after('fondo');
            $table->string('instragram')->nullable()->after('facebook');
            $table->string('tiktok')->nullable()->after('instragram');
            $table->string('direccion')->nullable()->after('tiktok');
            $table->string('region')->nullable()->after('direccion');
            $table->string('comuna')->nullable()->after('region');
            $table->string('telefono')->nullable()->after('comuna');
            $table->string('celular')->nullable()->after('telefono');
            $table->string('email')->nullable()->after('celular');
            $table->string('sitio')->nullable()->after('email');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emp_empresas', function ($table) {
            $table->dropColumn('logo');
            $table->dropColumn('fondo');
            $table->dropColumn('facebook');
            $table->dropColumn('instragram');
            $table->dropColumn('tiktok');
            $table->dropColumn('direccion');
            $table->dropColumn('region');
            $table->dropColumn('comuna');
            $table->dropColumn('telefono');
            $table->dropColumn('celular');
            $table->dropColumn('email');
            $table->dropColumn('sitio');
        });
    }
};
