<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_boletas', function (Blueprint $table) {
            $table->id();
            $table->string('folio');
            $table->decimal('monto_total');
            $table->integer('cantidades')->default(0);
            $table->date('fecha_de_compra');
            $table->unsignedBigInteger('usuario_id')->nullable();
            $table->foreign('usuario_id')->references('id')->on('portal_usuarios');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**compras_usuario
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_boletas');
    }
};
