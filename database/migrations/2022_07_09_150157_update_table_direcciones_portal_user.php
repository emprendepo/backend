<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal_usuarios_direcciones', function ($table) {
            $table->string('region')->after('comuna');
        });
    }

    public function down()
    {
        Schema::table('portal_usuarios_direcciones', function ($table) {
            $table->dropColumn('region');
        });
    }
};
