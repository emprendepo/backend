<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('emp_detalle_ventas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('ventas_id');
            $table->foreign('ventas_id')->references('id')->on('emp_ventas');
            $table->unsignedBigInteger('productos_id');
            $table->foreign('productos_id')->references('id')->on('emp_productos');
            $table->integer('cantidad');
            $table->integer('valor');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emp_detalle_ventas');
    }
};
