<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_carros_de_compras_detalle', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('carros_de_compras_id');
            $table->foreign('carros_de_compras_id')->references('id')->on('portal_carros_de_compras');
            $table->unsignedBigInteger('emp_productos_id');
            $table->foreign('emp_productos_id')->references('id')->on('emp_productos');
            $table->integer('cantidad')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_carros_de_compras_detalle');
    }
};
