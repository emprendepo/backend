<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('table_pedidos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('boletas_id')->unique();
            $table->foreign('boletas_id')->references('id')->on('emp_boletas');
            $table->unsignedBigInteger('empresas_id');
            $table->foreign('empresas_id')->references('id')->on('emp_empresas');
            $table->integer('cantidades')->default(0);
            $table->integer('valor_total')->default(0);
            $table->string('direccion');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_pedidos');
    }
};
