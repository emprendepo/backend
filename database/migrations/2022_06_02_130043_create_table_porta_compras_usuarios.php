<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portal_compras_usuarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('portal_boletas_id')->nullable();
            $table->foreign('portal_boletas_id')->references('id')->on('portal_boletas');
            $table->unsignedBigInteger('direccion_id')->nullable();
            $table->foreign('direccion_id')->references('id')->on('portal_usuarios_direcciones');
            $table->json('result')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portal_compras_usuarios');
    }
};
