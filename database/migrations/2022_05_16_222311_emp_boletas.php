<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        //Folio, precio neto, precio con IVA, costo IVA, fecha de venta, hora de venta, cliente involucrado, trabajador involucrado, método de pago.
        Schema::create('emp_boletas', function (Blueprint $table) {
            $table->id();
            $table->string('folio');
            $table->decimal('precio_neto');
            $table->decimal('precio_con_iva');
            $table->decimal('costo_iva');
            $table->date('fecha_de_venta');
            $table->unsignedBigInteger('clientes_id');
            $table->foreign('clientes_id')->references('id')->on('emp_clientes');
            $table->unsignedBigInteger('metodo_de_pago_id');
            $table->foreign('metodo_de_pago_id')->references('id')->on('emp_metodo_de_pago');
            $table->unsignedBigInteger('empresas_id');
            $table->foreign('empresas_id')->references('id')->on('emp_empresas');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('emp_boletas');
    }
};
