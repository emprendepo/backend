<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portal_carros_de_compras', function ($table) {
            $table->json('data')->nullable()->after('usuario_id');
            $table->json('result')->nullable()->after('data');
            $table->string('token')->nullable()->after('result');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portal_carros_de_compras', function ($table) {
            $table->dropColumn('data');
            $table->dropColumn('result');
            $table->dropColumn('token');
        });
    }
};
